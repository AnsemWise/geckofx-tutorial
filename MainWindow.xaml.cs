using Gecko;
using System;
using System.IO;
using System.Windows;
using System.Windows.Forms.Integration;
using System.Configuration;
using System.Threading.Tasks;

namespace YouTube_Ribcage
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            xulStarter();
        }
        private async void startXul(string randAgent, int viewTime)
        {

            var path = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "xulrunner");
            Gecko.Xpcom.Initialize(path);
            WindowsFormsHost host = new WindowsFormsHost();
            GeckoWebBrowser browser = new GeckoWebBrowser();
            Gecko.GeckoPreferences.User["general.useragent.override"] = randAgent;
            host.Child = browser;
            gridWeb.Children.Add(host);
            browser.Navigate("Your website");
            await PutTaskDelay(viewTime);
        }

        private void xulStarter()
        {
            int selAgent;
            string numOfAgentStr = ConfigurationManager.AppSettings.Get("user-agent");
            string[] userAgents = numOfAgentStr.Split('|');

            string viewTimeStr = ConfigurationManager.AppSettings.Get("ViewTime");
            int viewTime = Int32.Parse(viewTimeStr.Trim());
            Random rnd = new Random();
            selAgent = rnd.Next(0, (userAgents.Length));

            // v this is the important line for tasking
            Parallel.Invoke(() => startXul(userAgents[selAgent], viewTime));
        }

        async Task PutTaskDelay(int delay)
        {
            await Task.Delay(delay);
        }
    }
}
