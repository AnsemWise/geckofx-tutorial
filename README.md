# README #

### What is this repository for? ###

* Getting started with GeckoFX

### How do I get set up? ###

1. Go to this tutorial [GeckoFX Tutorial](https://nhabuiduc.wordpress.com/2014/09/18/geckofx-net-webbrowser-setup-and-features/)
2. Make sure your GeckoFX and XulRunner versions are 33.*
3. Don't try and add "Geckofx-Core and Geckofx-Winforms" as references
4. Open Nuget package manager and search for GeckoFX, then install "Geckofx-Core" and "Geckofx-Winforms", version 33.*
5. Then search for async and install Microsoft.Bcl.Build, Microsoft.Bcl.Async, Microsoft.Bcl
6. Put the copy the files content into a blank WPF application and then edit the portions I pointed out.

Ask questions in the issue reporter, skype, or pm me on the forums. Good Luck!
